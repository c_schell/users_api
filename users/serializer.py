from rest_framework import serializers
from .models import User, Log

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('id','email','name','created_at','updated_at')

	def update(self, valid_user, **kwargs):
		for field, value in kwargs.items():
			if field != 'id' and field != 'created_at' and field != 'updated_at':
				setattr(valid_user, field, value)
		valid_user.save()

	def save(self, valid_data):
		user = User.objects.create(**valid_data)
		user.save()
		return user

	def replay(self,valid_user, **kwargs):
		for field, value in kwargs.items():
			if field != 'id' and field != 'created_at' and field != 'updated_at':
				setattr(valid_user, field, value)
		valid_user.save(updated_at=kwargs.get('updated_at'))

	def recreate(self, **valid_data):
		valid_data['id'] = valid_data.get('id')
		valid_data['created_at'] = valid_data.get('created_at')
		user = User.objects.create(**valid_data)
		user.save(updated_at=valid_data.get('updated_at'))
		return user

class LogSerializer(serializers.ModelSerializer):
	class Meta:
		model = Log
		fields = ('id','user_id','action','attributes','created_at')