# Generated by Django 2.0.5 on 2018-05-08 04:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_log_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='log',
            name='user_id',
            field=models.IntegerField(null=True),
        ),
    ]
