# Generated by Django 2.0.5 on 2018-05-07 19:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_logger'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='data',
            field=models.TextField(null=True),
        ),
    ]
