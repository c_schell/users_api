from django.urls import path
from . import views

urlpatterns = [
	path('', views.api_index, name='api_index'),
	path('users/', views.users_list_and_add, name='users_list_and_add'),
	path('users/<int:pk>/', views.user_get_and_update, name='user_get_and_update'),
	path('log/', views.get_logs, name='get_logs'),
	path('log/<int:fk>/', views.get_log, name='get_log'),
	path('log/replay/', views.log_replay, name='log_replay'),
]