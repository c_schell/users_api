from django.db import models
from django.utils import timezone
from django.urls import reverse

class User(models.Model):
	email = models.EmailField(max_length=254)
	name = models.CharField(max_length=100)
	created_at = models.DateTimeField(default=timezone.now,editable=False)
	updated_at = models.DateTimeField(default=timezone.now)

	def save(self, *args, **kwargs):
		if kwargs.get('updated_at'):
			self.updated_at = kwargs.get('updated_at')
			del kwargs['updated_at']
		else:
			self.updated_at = timezone.now()
		return super(User, self).save(*args, **kwargs)

	def get_absolute_url(self):
		return reverse('user_get_and_update', args=[str(self.id)])

	def get_browser_url(self):
		return reverse('user_detail_view', args=[str(self.id)])

	def __str__(self):
		return f'{self.id}'

class Log(models.Model):
	user_id = models.IntegerField(null=True)
	action = models.CharField(max_length=6, null=True)
	attributes = models.TextField(null=True)
	created_at = models.DateTimeField(default=timezone.now,editable=False)
	data = models.TextField(null=True)

	def __str__(self):
		return f'{self.id}'