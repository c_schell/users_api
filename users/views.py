from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import status
from users.models import User, Log
from users.serializer import UserSerializer, LogSerializer
from django.utils import timezone
from django.utils.six import BytesIO
import json
import logging


class JSONResponse(HttpResponse):
	def __init__(self, data, **kwargs):
		content = JSONRenderer().render(data)
		kwargs['content_type'] = 'application/json'
		super(JSONResponse, self).__init__(content, **kwargs)

class Logger:
	def __init__(self, user):
		self.entry = Log.objects.create()
		self.user = user
		self.entry.user_id = user.id
	def set(self,**kwargs):
		self.entry.action = kwargs.get('action')
		self.entry.attributes = kwargs.get('attributes')
		if self.entry.action != 'delete':
			self.entry.data = kwargs.get('data')
			self.entry.data['updated_at'] = str(self.user.updated_at)
			self.entry.data['created_at'] = str(self.user.created_at)
		self.entry.save()
	def disk_format(**kwargs):
		return '{}: USER {} | LOG {}'.format(kwargs.get("action").upper(),kwargs.get("user_id"),kwargs.get("log_id"))

disk_logger = logging.getLogger(__name__)
disk_logger_template = Logger.disk_format

@csrf_exempt
def api_index(request):
	return HttpResponse(status=status.HTTP_404_NOT_FOUND)

@csrf_exempt
def users_list_and_add(request):
	if request.method == 'GET':
		users = User.objects.all()
		users_serialized = UserSerializer(users, many=True)
		return JSONResponse(users_serialized.data)
	elif request.method == 'POST':
		user_data = JSONParser().parse(request)
		user_serialized = UserSerializer(data=user_data)
		if user_serialized.is_valid():
			new_user = user_serialized.save(user_serialized.data)
			log = Logger(new_user)
			log.set(action='create',attributes=new_user.get_absolute_url(),data=user_serialized.data)
			disk_logger.info(disk_logger_template(action='create',user_id=new_user.id,log_id=log.entry.id))
			return JSONResponse(user_serialized.data, status=status.HTTP_201_CREATED)
		return JSONResponse(user_serialized.errors, status=status.HTTP_400_BAD_REQUEST)
	return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

@csrf_exempt
def user_get_and_update(request,pk):
	try:
		user = User.objects.get(id=pk)
	except User.DoesNotExist:
		return HttpResponse(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		user_serialized = UserSerializer(user)
		return JSONResponse(user_serialized.data)

	log = Logger(user)

	if request.method == 'PATCH':
		new_user_data = JSONParser().parse(BytesIO(request.body))
		user_serialized = UserSerializer(user, data=new_user_data, partial=True)
		if user_serialized.is_valid():
			user_serialized.update(user,**new_user_data)
			log.set(action='update',attributes=user.get_absolute_url(),data=new_user_data)
			disk_logger.info(disk_logger_template(action='update',user_id=user.id,log_id=log.entry.id))
			return HttpResponse(status=status.HTTP_204_NO_CONTENT)
		return JSONResponse(user_serialized.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		log.set(action='delete',attributes=user.get_absolute_url(),data={})
		disk_logger.info(disk_logger_template(action='delete',user_id=user.id,log_id=log.entry.id))
		user.delete()
		return HttpResponse(status=status.HTTP_200_OK)
	return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

@csrf_exempt
def get_logs(request):
	try:
		logs = Log.objects.all()
		logs_serialized = LogSerializer(logs, many=True)
		return JSONResponse(logs_serialized.data)
	except:
		HttpResponse(status=status.HTTP_400_BAD_REQUEST)

@csrf_exempt
def get_log(request,fk):
	try:
		logs = Log.objects.filter(user_id=fk)
		logs[0]
	except IndexError:
		return HttpResponse(status=status.HTTP_404_NOT_FOUND)
	log_serialized = LogSerializer(logs, many=True)
	return JSONResponse(log_serialized.data)


@csrf_exempt
def log_replay(request):
	if request.method == "POST":
		try:
			log = JSONParser().parse(BytesIO(request.body))
		except:
			return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
		try:
			log = Log.objects.get(id=log['id'])
		except:
			return HttpResponse(status=status.HTTP_404_NOT_FOUND)
		if log.action == 'update':
			user = User.objects.get(id=log.user_id)
			log_entry = Logger(user)
			log_data = json.loads(log.data.replace("'",'"'))
			user_serialized = UserSerializer(user, data=log_data, partial=True)
			if user_serialized.is_valid():
				user_serialized.replay(user,**log_data)
				log_entry.set(action='update',attributes=user.get_absolute_url(),data=log_data)
				disk_logger.info(disk_logger_template(action='update',user_id=user.id,log_id=log_entry.id))
			return HttpResponse(status=status.HTTP_204_NO_CONTENT)
		if log.action == 'create':
			log_data = json.loads(log.data.replace("'",'"'))
			user_serialized = UserSerializer(data=log_data)
			if user_serialized.is_valid():
				new_user = user_serialized.recreate(**user_serialized.data,id=log.user_id,created_at=log_data['created_at'])
				log_entry = Logger(new_user)
				log_entry.set(action='create',attributes=new_user.get_absolute_url(),data=log_data)
				disk_logger.info(disk_logger_template(action='create',user_id=new_user.id,log_id=log_entry.entry.id))
				return JSONResponse(user_serialized.data, status=status.HTTP_201_CREATED)
			return JSONResponse(user_serialized.errors, status=status.HTTP_400_BAD_REQUEST)
		if log.action == 'delete':
			user = User.objects.get(id=log.user_id)
			log_entry = Logger(user)
			log_entry.set(action='delete',attributes=user.get_absolute_url())
			disk_logger.info(disk_logger_template(action='delete',user_id=user.id,log_id=log_entry.id))
			user.delete()
			return HttpResponse(status=status.HTTP_200_OK)
	return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)
			

