from django.test import TestCase
from django.urls import reverse
from .serializer import UserSerializer,LogSerializer
from .models import User, Log


class UsersAPITests(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            name='test name',
            email='test@example.com',
        )

        self.log = Log.objects.create(
            user_id=self.user.id,
            action='create',
            attributes=self.user.get_absolute_url()
        )

        self.user_serialized = User.objects.create(
        	name='test serializer',
        	email='test_serializer@example.com',
        )
        self.user_serialized = UserSerializer(self.user_serialized)

        self.log_serialized = Log.objects.create(
        	user_id=self.user_serialized.data['id'],
        	action='create',
        	attributes='/api/users/{}'.format(self.user_serialized.data['id']),
        	created_at=self.user_serialized.data['created_at']
        )
        self.log_serialized = LogSerializer(self.log_serialized)

    def test_string_representation(self):
        self.assertEqual(self.user.__str__(), str(self.user.id))
        self.assertEqual(self.log.__str__(), str(self.log.id))

    def test_user_serializer_attributes(self):
    	data = self.user_serialized.data
    	self.assertEqual(set(data.keys()), set(['email', 'id','created_at','name','updated_at']))

    def test_log_serializer_attributes(self):
    	data = self.log_serialized.data
    	self.assertEqual(set(data.keys()), set(['id','user_id','attributes','action','created_at']))

    def test_get_absolute_url(self):
        self.assertEquals(self.user.get_absolute_url(), '/api/users/1/')

    def test_user_is_set(self):
        self.assertEqual('{}'.format(self.user.id), str(1))
        self.assertEqual('{}'.format(self.user.name), 'test name')
        self.assertEqual('{}'.format(self.user.email), 'test@example.com')

    def test_user_save_method(self):
    	old_updated_at = self.user.updated_at
    	old_created_at = self.user.created_at
    	self.user.save()
    	self.assertNotEqual(self.user.updated_at,old_updated_at)
    	self.user.save(updated_at=old_updated_at)
    	self.assertEqual(str(self.user.updated_at),str(old_updated_at))
    	self.assertEqual(self.user.created_at,old_created_at)

    def test_log_is_set(self):
    	self.assertEqual('{}'.format(self.log.id), str(1))
    	self.assertEqual('{}'.format(self.log.user_id), str(1))
    	self.assertEqual('{}'.format(self.log.action), 'create')
    	self.assertEqual('{}'.format(self.log.attributes), '/api/users/1/')

    def test_users_get(self):
    	response = self.client.get(reverse('users_list_and_add'))
    	json_response = response.json()[0]
    	self.assertIn('id',json_response)
    	self.assertIn('name',json_response)
    	self.assertIn('email',json_response)
    	self.assertIn('created_at',json_response)
    	self.assertIn('updated_at',json_response)
    	self.assertEqual(response.status_code, 200)

    def test_users_and_logs_invalid(self):
    	response = self.client.get('/api/users/1000/')
    	self.assertEqual(response.status_code, 404)
    	response = self.client.get('/api/')
    	self.assertEqual(response.status_code, 404)
    	response = self.client.get('/api/log/1000/')
    	self.assertEqual(response.status_code, 404)
    	response = self.client.patch('/api/users/')
    	self.assertEqual(response.status_code, 405)
    	response = self.client.delete('/api/log/replay/')
    	self.assertEqual(response.status_code, 405)
    	response = self.client.post('/api/users/1/',data='{"name":"new user","email":"new_user@example.com"}',content_type="application/json")
    	self.assertEqual(response.status_code, 405)

    def test_users_new_post(self):
    	response = self.client.post("/api/users/",data='{"name":"new user","email":"new_user@example.com"}',content_type="application/json")
    	self.assertEqual(response.status_code, 201)

    def test_logs_user_post(self):
    	post_data='{{"id":"{}","user_id":{},"action":"{}","attributes":"{}","created_at":"{}"}}'.format(self.log.id,self.log.user_id,self.log.action,self.log.attributes,str(self.log.created_at))
    	response = self.client.post('/api/log/',data=post_data, content_type="application/json")
    	self.assertEqual(response.status_code, 200)

    def test_users_individual_get(self):
    	response = self.client.get(self.user.get_absolute_url())
    	self.assertEqual(response.status_code, 200)
    	self.assertEqual(response.json()['id'], self.user.id)

    def test_users_individual_delete(self):
    	user2 = User.objects.create(
            name='test name jr',
            email='test_jr@example.com',
        )
    	response = self.client.delete(user2.get_absolute_url())
    	self.assertEqual(response.status_code, 200)
    	delete_log = Log.objects.filter(user_id=user2.id)[0]
    	post_data='{{"id":"{}","user_id":{},"action":"{}","attributes":"{}","created_at":"{}"}}'.format(delete_log.id,delete_log.user_id,delete_log.action,delete_log.attributes,str(delete_log.created_at))
    	response = self.client.post('/api/log/',data=post_data, content_type="application/json")
    	self.assertEqual(response.status_code, 200)

    def test_users_individual_patch(self):
    	user3 = User.objects.create(
            name='test name III',
            email='test_III@example.com',
        )
    	response = self.client.patch(user3.get_absolute_url(),data='{"name":"name changed","email":"name_changed@example.com"}',content_type="application/json")
    	self.assertEqual(response.status_code, 204)
    	patch_log = Log.objects.filter(user_id=user3.id)[0]
    	post_data='{{"id":"{}","user_id":{},"action":"{}","attributes":"{}","created_at":"{}"}}'.format(patch_log.id,patch_log.user_id,patch_log.action,patch_log.attributes,str(patch_log.created_at))
    	response = self.client.post('/api/log/',data=post_data, content_type="application/json")
    	self.assertEqual(response.status_code, 200)

    def test_logs_get(self):
    	response = self.client.get('/api/log/')
    	self.assertEqual(response.status_code, 200)
    	self.assertEqual(response.json()[0]['id'], 1)

    def test_logs_get_user_history(self):
    	response = self.client.get('/api/log/{}/'.format(self.user.id))
    	self.assertEqual(response.status_code, 200)
    	self.assertEqual(response.json()[0]['id'], self.log.id)

    def test_templates_used(self):
    	response = self.client.get('/')
    	self.assertEqual(response.status_code, 200)
    	self.assertTemplateUsed(response, 'index.html')
    	response = self.client.get('/users/')
    	self.assertEqual(response.status_code, 200)
    	self.assertTemplateUsed(response, 'users.html')
    	response = self.client.get('/log/')
    	self.assertEqual(response.status_code, 200)
    	self.assertTemplateUsed(response, 'logs.html')
    	response = self.client.get('/users/1/')
    	self.assertEqual(response.status_code, 200)
    	self.assertTemplateUsed(response, 'user_detail.html')
    	response = self.client.get('/log/replay/')
    	self.assertEqual(response.status_code, 200)
    	self.assertTemplateUsed(response, 'log_replay.html')
    	response = self.client.get('/log/1/')
    	self.assertEqual(response.status_code, 200)
    	self.assertTemplateUsed(response, 'log_detail.html')
    	response = self.client.get('/users/new/')
    	self.assertEqual(response.status_code, 200)
    	self.assertTemplateUsed(response, 'user_new.html')








