from django import forms

class UserForm(forms.Form):
	name = forms.CharField(max_length=100)
	email = forms.EmailField(max_length=254,required=False)
	delete = forms.BooleanField(required=False)

class LogForm(forms.Form):
	id = forms.IntegerField(label='Log Id to replay')