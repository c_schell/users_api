from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from users.models import User, Log
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .forms import UserForm, LogForm
from django.urls import reverse_lazy, reverse
import requests
from django import forms
import json

def index(request):
	return render(
		request,
		'index.html'
	)

class UsersListView(ListView):
	model = User
	template_name = 'users.html'
	context_object_name = 'users_list'
	paginate_by = 20

class NewUserForm(forms.Form):
	name = forms.CharField(max_length=100)
	email = forms.EmailField(max_length=254)

def UserCreateView(request):
	form = NewUserForm(request.POST)
	if form.is_valid():
		name = form.cleaned_data['name']
		email = form.cleaned_data['email']
		data = json.dumps({'name':name,'email':email})
		requests.post('http://127.0.0.1:8000/api/users/', data)
		return HttpResponseRedirect(reverse_lazy('index'))
	return render(request,'user_new.html',{'form':form})

# class UserCreateView(CreateView):
# 	model = User
# 	template_name = 'user_new.html'
# 	fields = ['name','email']
# 	success_url = reverse_lazy('index')

class LogListView(ListView):
	model = Log
	template_name = 'logs.html'
	context_object_name = 'log_list'
	paginate_by = 20

def LogDetailView(request, pk):
	user_logs = Log.objects.filter(user_id=pk)
	try:
		user_logs[0]
		context = {'user_logs':user_logs}
	except IndexError:
		context = {}
	return render(request,'log_detail.html',context)

def UserDetailView(request,pk):
	try:
		user = User.objects.get(id=pk)
	except User.DoesNotExist:
		return redirect(reverse_lazy('index'))
	form = UserForm(request.POST)
	if form.is_valid():
		if form.cleaned_data['delete']:
			data = json.dumps({'id':user.id})
			requests.delete('http://127.0.0.1:8000/api/users/{}'.format(user.id))
			return HttpResponseRedirect(reverse_lazy('index'))
		data = {}
		name = form.cleaned_data['name']
		data['name'] = name
		if form.cleaned_data['email'] != '':
			email = form.cleaned_data['email']
			data['email'] = email
		data = json.dumps(data)
		print(data)
		requests.patch('http://127.0.0.1:8000/api/users/{}/'.format(user.id),data)
		return HttpResponseRedirect(reverse_lazy('index'))
	return render(request, 'user_detail.html', {'form':form,'user':user})

def LogReplayView(request):
	form = LogForm(request.POST)
	if form.is_valid():
		id = form.cleaned_data['id']
		data = json.dumps({'id':id})
		requests.post('http://127.0.0.1:8000/api/log/replay/', data)
		return HttpResponseRedirect(reverse_lazy('index'))
	return render(request, 'log_replay.html', {'form':form})






