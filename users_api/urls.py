"""users_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import path, include
from django.contrib import admin
from django.contrib.staticfiles.views import serve
from django.views.generic import RedirectView
from . import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('users/', views.UsersListView.as_view(), name='users_list'),
    path('users/<int:pk>/', views.UserDetailView, name='user_detail_view'),
    path('users/new/', views.UserCreateView, name='user_create'),
    url(r'^log/$', views.LogListView.as_view(), name='log_list'),
    path('log/<int:pk>/', views.LogDetailView, name='log_detail_view'),
    path('log/replay/', views.LogReplayView, name='log_replay_view'),
    path('api/', include('users.urls')),
]